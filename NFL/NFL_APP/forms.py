from django import forms
from .models import Equipos, Jugadores, Estadios

class EquiposForm(forms.ModelForm):
    class Meta:
        model= Equipos
        fields='__all__'


class JugadoresForm(forms.ModelForm):
    class Meta:
        model= Jugadores
        fields='__all__'


class EstadiosForm(forms.ModelForm):
    class Meta:
        model= Estadios
        fields='__all__'