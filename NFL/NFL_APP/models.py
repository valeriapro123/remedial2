
from django.db import models


class Equipos(models.Model):
    nombre = models.CharField(max_length=255)
    dueño = models.CharField(max_length=100)
    evento = models.CharField(max_length=255)


def __str__(self):
    fila = "Nombre:" + self.nombre + "Dueño:" + self.dueño + "Evento:" + self.evento
    return fila



class Jugadores(models.Model):
    nombre = models.CharField(max_length=255)
    numero = models.IntegerField()
    posicion = models.CharField(max_length=50)
    equipo = models.CharField(max_length=255)


def __str__(self):
    fila = "Nombre:" + self.nombre + "Numero:" + self.numero + "Posicion:" + self.posicion + "Equipo:" + self.equipo
    return fila




class Estadios(models.Model):
    nombre = models.CharField(max_length=255)
    capacidad = models.CharField(max_length=255)
    tamaño = models.CharField(max_length=50)


def __str__(self):
    fila = "Nombre:" + self.nombre + "Capacidad:" + self.capacidad + "Tamaño:" + self.tamaño
    return fila



