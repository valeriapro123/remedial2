# Generated by Django 4.2.7 on 2023-12-05 16:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Equipos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('dueño', models.CharField(max_length=100)),
                ('evento', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Estadios',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('capacidad', models.IntegerField()),
                ('tamaño', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Jugadores',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=255)),
                ('numero', models.IntegerField()),
                ('posicion', models.CharField(max_length=50)),
                ('equipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='NFL_APP.equipos')),
            ],
        ),
    ]
