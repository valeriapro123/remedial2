# Generated by Django 4.2.7 on 2023-12-06 10:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NFL_APP', '0002_alter_jugadores_equipo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estadios',
            name='capacidad',
            field=models.CharField(max_length=255),
        ),
    ]
