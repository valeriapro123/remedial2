from django.urls import path
from . import views

urlpatterns = [
    path ('', views.inicio, name='inicio'),
    path ('nosotros', views.nosotros, name='nosotros'),



    path ('equipos', views.equipos, name='equipos'),
    path ('equipos/crear', views.crear, name='crear'),
    path ('eliminar/<int:id>', views.eliminar, name='eliminar'),
    path ('equipos/editar/<int:id>', views.editar, name='editar'),


    path ('jugadores', views.jugadores, name='jugadores'),
    path ('jugadores/crearjugador', views.crearjugador, name='crearjugador'),
    path ('eliminar/jugadores/<int:id>', views.eliminarjugador, name='eliminarjugador'),
    path ('jugadores/editarjugador/<int:id>', views.editarjugador, name='editarjugador'),



    path ('estadios', views.estadios, name='estadios'),
    path ('estadios/crearestadio', views.crearestadio, name='crearestadio'),
    path ('eliminar/estadios/<int:id>', views.eliminarestadio, name='eliminarestadio'),
    path ('estadios/editarestadio/<int:id>', views.editarestadio, name='editarestadio'),


]


from django.conf import settings
