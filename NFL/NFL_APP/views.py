from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Equipos, Jugadores, Estadios
from .forms import EquiposForm, JugadoresForm, EstadiosForm


def inicio (request):
    return render(request, 'paginas/inicio.html')
def nosotros(request):
    return render(request, 'paginas/nosotros.html')


def equipos(request):
    equipos = Equipos.objects.all()
    print(equipos)
    return render(request, 'equipos/index.html',{'equipos':equipos})

def crear(request):
    formulario = EquiposForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/crear.html', {'formulario': formulario})

def editar(request, id):
    equipos = Equipos.objects.get(id=id)
    formulario = EquiposForm(request.POST or None, request.FILES or None, instance=equipos)
    if formulario.is_valid() and request.POST:
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/editar.html', {'formulario':formulario})


def eliminar(request, id):
    equipos = Equipos.objects.get(id=id)
    equipos.delete()
    return redirect('equipos')



def jugadores(request):
    jugadores = Jugadores.objects.all()
    print(jugadores)
    return render(request, 'jugadores/index.html',{'jugadores':jugadores})


def crearjugador(request):
    formulario = JugadoresForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('jugadores')
    return render(request, 'jugadores/crearjugador.html', {'formulario': formulario})

def editarjugador(request, id):
    jugadores = Jugadores.objects.get(id=id)
    formulario = JugadoresForm(request.POST or None, request.FILES or None, instance=jugadores)
    if formulario.is_valid() and request.POST:
        formulario.save()
        return redirect('jugadores')
    return render(request, 'jugadores/editarjugador.html', {'formulario':formulario})


def eliminarjugador(request, id):
    jugadores = Jugadores.objects.get(id=id)
    jugadores.delete()
    return redirect('jugadores')




def estadios(request):
    estadios = Estadios.objects.all()
    print(estadios)
    return render(request, 'estadios/index.html',{'estadios':estadios})


def crearestadio(request):
    formulario = EstadiosForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('estadios')
    return render(request, 'estadios/crearestadio.html', {'formulario': formulario})

def editarestadio(request, id):
    estadios = Estadios.objects.get(id=id)
    formulario = EstadiosForm(request.POST or None, request.FILES or None, instance=estadios)
    if formulario.is_valid() and request.POST:
        formulario.save()
        return redirect('estadios')
    return render(request, 'estadios/editarestadio.html', {'formulario':formulario})


def eliminarestadio(request, id):
    estadios = Estadios.objects.get(id=id)
    estadios.delete()
    return redirect('estadios')