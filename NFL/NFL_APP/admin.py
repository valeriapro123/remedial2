from django.contrib import admin
from .models import Equipos, Jugadores, Estadios

# Register your models here.

admin.site.register(Equipos)
admin.site.register(Jugadores)
admin.site.register(Estadios)